class User < ApplicationRecord
  has_secure_password
  validates :email, length: {maximum: 255}
  validates :password, presence: true, length: { minimum: 6 }
end
